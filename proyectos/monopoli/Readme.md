# monopoly

Una implementación de JavaScript / HTML / CSS Monopoly con juego completo. Admite dos y ocho jugadores.

Incluye una capacidad experimental para jugar contra una IA. Se incluye una prueba de IA para fines de demostración.
