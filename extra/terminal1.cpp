
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 8080 


int time_token;
char time_token_str[10];

void dostuff(int);
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{

     int sockfd, newsockfd, portno, pid, terminal_id;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;



 

  

    if (argc != 2){
        printf("Forma de uso: %s tiempo_segundos \n", argv[0]);
        exit(0);
    }

    time_token = atoi(argv[1]);
    sprintf(time_token_str, "%d", time_token);

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR abriendo el socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = PORT;
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR en bind");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     while (1) {
         newsockfd = accept(sockfd, 
               (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0) 
             error("ERROR en aceptar");
         pid = fork();
         if (pid < 0)
             error("ERROR en fork");
         if (pid == 0)  {
             close(sockfd);
             dostuff(newsockfd);
             exit(0);
         }
         else close(newsockfd);
     } 
     close(sockfd);
     return 0; 
}


void dostuff (int sock)
{
   int n;
   char buffer[256];
      
   bzero(buffer,256);
   n = read(sock,buffer,255);
   if (n < 0) error("ERROR leyendo del socket");

   printf("Programa finalizado en terminal %s\n",buffer);



   n = write(sock,time_token_str,sizeof(time_token_str));
   if (n < 0) error("ERROR escribiendo en el socket");


}
