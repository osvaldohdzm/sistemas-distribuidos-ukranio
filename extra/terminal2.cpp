#include <algorithm> 

#include <bits/stdc++.h>
#include <ctime> 
#include <chrono> 
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <netdb.h> 
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <thread>
#include <time.h>
#include <unistd.h>
#include <vector>
#define PORT 8080 

using namespace std;
using namespace std::chrono; 
clock_t tStart;
int time_execution = 0;
int max_execution_time;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    vector<int> values(10000); 
    auto f = []() -> int { return rand() % 10000; }; 
    generate(values.begin(), values.end(), f);   
    auto start = high_resolution_clock::now(); 
    sort(values.begin(), values.end()); 


    char terminal_id[] = "3";
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 1) {
       fprintf(stderr,"usage %s\n", argv[0]);
       exit(0);
    }
    portno =  PORT;;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname("127.0.0.1");
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    bzero(buffer,256);
    memcpy ( buffer, terminal_id, sizeof(terminal_id) );
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) 
         error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
    close(sockfd);


    auto stop = high_resolution_clock::now(); 
    auto duration = duration_cast<microseconds>(stop - start);
    time_execution= duration.count()/1000000;
    
    


         if (time_execution < max_execution_time)
         {
             printf("\nPasando a siguiente terminal...\n");
         }
         else
         {
               cout << "Tiempo de ejecución: " << time_execution << " segundos" << endl; 
         }
      

    return 0;
}