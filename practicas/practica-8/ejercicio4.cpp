#include <iostream>
#include <unistd.h>
#include <thread>
#include <atomic> 

using namespace std;

atomic<int> global;

void funcionIncremento(int valor) {	
	for(int i = 1;i <= valor; i++)
		global = global + 1;
	
	//sleep(1);
}

void funcionDecremento(int valor) {
	for(int i = 1;i <= valor; i++)
		global = global - 1;
	
	//sleep(1);
}

int main() {
	int inconsistencia = 0;
	for(int valor = 1;valor <= 100000; valor++){
		thread th1(funcionIncremento, valor), th2(funcionDecremento, valor); 
		th1.join();
		th2.join();
		if(global !=0)
			inconsistencia++;
	}
		cout << "Numero de inconsistencias: " << inconsistencia << endl;

		cout << "Valor de variable global: " << global << " veces" << endl;
		
	exit(0);
}


