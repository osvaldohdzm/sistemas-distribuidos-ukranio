#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <thread>
using namespace std;

int g=0;

void func1(){
	g++;
	sleep(1);

}

void func2(){
	g--;
	sleep(1);
}


int main()
{
	thread th1(func1), th2(func2);
	th1.join();		
	th2.join();
	cout<< g << endl;
	return 0;
}