#include <iostream>
#include <unistd.h>
#include <thread>
using namespace std;

void funcion(int valor){
	printf("\nHilo %ld  Valor recibido:  %d", this_thread::get_id(), valor);
	sleep(2);
}
int main(){
	thread th1(funcion, 5), th2(funcion, 10);
	cout << "Proceso principal espera que los hilos terminen\n";
	th1.join();
	th2.join();
	cout << "\nEl hilo principal termina\n";
	exit(0);
}
