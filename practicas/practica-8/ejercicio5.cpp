#include <iostream>
#include <unistd.h>
#include <thread>
#include <mutex>
using namespace std;

/*USO DE SEMAFOROS BINARIOS	

********* Funciones ****************
m.lock(); //Para bloquear la ejecución
//Aquí va el código de la región crítica
m.unlock(); //Para desbloquear la ejecución
*/
mutex m;
int variableGlobal = 0;


void funcionIncremento(int valor)
{	
	int i;
	m.lock();
	for(i=1;i<=valor;i++){
		
		variableGlobal= variableGlobal+1;		
	}
	m.unlock();
	sleep(1);
}

void funcionDecremento(int valor)
{
	int i;
	m.lock();
	for(i=1;i<=valor;i++){
		variableGlobal= variableGlobal-1;
	}
	m.unlock();
	sleep(1);
}

int main()
{
	int valor;
	
	
	for(valor=1;valor<=10;valor++){

		thread th1(funcionIncremento, valor), th2(funcionDecremento, valor);
		//cout << "Proceso principal espera que los hilos terminen\n";
		th1.join();
		th2.join();
		//cout << "El hilo principal termina\n";

		//cout << "variableGlobal: " << variableGlobal <<endl;
		
		if(variableGlobal!=0){
			//cout << "Se incremento: " << valor << "veces hasta la inconsistencia" << endl;
			break;
		}
	}
	cout << "Se incremento: " << valor << " veces sin inconsistencia" << endl;
	
	exit(0);
}

/*SI SE COMPARTE LA VARIABLE */
