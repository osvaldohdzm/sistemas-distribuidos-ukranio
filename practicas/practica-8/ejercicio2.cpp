#include <iostream>
#include <unistd.h>
#include <thread>
using namespace std;

int variableGlobal = 0;

void funcionIncremento() {	
	printf("\nHilo %ld",this_thread::get_id());
	variableGlobal = variableGlobal + 1;
	sleep(1);
}

void funcionDecremento() {
	printf("\nHilo %ld",this_thread::get_id());
	variableGlobal = variableGlobal - 1;
	sleep(1);
}

int main() {
	thread th1(funcionIncremento), th2(funcionDecremento);
	cout << "\nProceso principal espera que los hilos terminen\n";
	th1.join();
	th2.join();
	cout << "\nEl hilo principal termina\n";
	cout << "El valor final de la variable global es: " << variableGlobal << endl;
	exit(0);
}


/*NO SE COMPARTE LA VARIABLE */
