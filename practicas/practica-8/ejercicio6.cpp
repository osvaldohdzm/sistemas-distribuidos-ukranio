#include "Semaforo.h"
#include <mutex>
#include <unistd.h>
#include <thread>
#include <iostream>

using namespace std;

int variableGlobal = 0;
Semaforo sem1, sem2;

void funcionIncremento(int valor) {	
	for(int i = 1;i <= valor; i++){
		sem1.wait();
		variableGlobal = variableGlobal + 1;	
		sem2.post();
	}
	//sleep(1);
}

void funcionDecremento(int valor) {
	for(int i = 1;i <= valor; i++){
		sem2.wait();
		variableGlobal = variableGlobal - 1;
		sem1.post();	
	}
	//sleep(1);
}

int main() {
	sem1.init(1);
	sem2.init(0);
	
	int inconsistencia = 0;
	for(int valor = 1;valor <= 10000; valor++){
		thread th1(funcionIncremento, valor), th2(funcionDecremento, valor);
		th1.join();
		th2.join();
		if(variableGlobal !=0)
			inconsistencia++;
	}
		cout << "Numero de inconsistencias: " << inconsistencia << endl;

		cout << "Valor de variable global: " << variableGlobal << " veces" << endl;
	
	exit(0);
}
