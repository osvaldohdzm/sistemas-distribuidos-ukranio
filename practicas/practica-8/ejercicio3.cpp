#include <iostream>
#include <unistd.h>
#include <thread>

using namespace std;

int variableGlobal = 0;

void funcionIncremento(int valor) {	
	for(int i = 1;i <= valor; i++){
		variableGlobal = variableGlobal + 1;
	}
	//sleep(1);
}

void funcionDecremento(int valor) {
	for(int i = 1;i <= valor; i++){
		variableGlobal = variableGlobal - 1;
	}
	//sleep(1);
}

int main() {
	int inconsistencia = 0;
	for(int valor = 1;valor <= 100000; valor++){
		thread th1(funcionIncremento, valor), th2(funcionDecremento, valor);
		th1.join();
		th2.join();
		if(variableGlobal !=0)
			inconsistencia++;
	}
		cout << "Numero de inconsistencias: " << inconsistencia << endl;

		cout << "Valor de variable global: " << variableGlobal << " veces" << endl;
		
	exit(0);
}

/*SI SE COMPARTE LA VARIABLE */
