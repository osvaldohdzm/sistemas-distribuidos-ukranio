#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <thread>
#include <sstream>

using namespace std;

void funcion(int valor){
	//cout<<"Hilo " <<  this_thread::get_id() << " Valor recibido: " << valor << endl;//3
	auto myid = this_thread::get_id();
	stringstream ss;
	ss<<myid;
	printf("Hilo %s Valor recibido: %d \n",ss.str().c_str(),valor);
	sleep(2);
}

int main(){
	thread th1(funcion, 5), th2(funcion, 10);//1
	printf("Proceso principal espera que los hilos terminen\n");
	//cout  << "Proceso principal espera que los hilos terminen\n";
	th1.join();//2
	th2.join();

	//cout << "El hilo principal termina\n";
	printf("El hilo principal termina\n");
	exit(0);
}