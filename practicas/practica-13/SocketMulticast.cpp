#include "SocketMulticast.h"

SocketMulticast::SocketMulticast(int puerto)
{
	s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	int reuse = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(reuse)) == -1) {
		printf("Error al llamar a la función setsockopt\n");
		exit(0);
	}
	/* rellena la dirección local */
   	bzero((char *)&direccionLocal, sizeof(direccionLocal));
   	direccionLocal.sin_family = PF_INET;
   	direccionLocal.sin_addr.s_addr = INADDR_ANY;
   	direccionLocal.sin_port = htons(puerto);
	bind(s, (struct sockaddr *)&direccionLocal, sizeof(direccionLocal));
	/* rellena la dirección foranea */
   	bzero((char *)&direccionForanea, sizeof(direccionForanea));
}

int SocketMulticast::recibe(PaqueteDatagrama & p){
	unsigned char inet[p.obtieneLongitud()];
	socklen_t clilen = sizeof(direccionForanea);
	int res= recvfrom(s, (char *)p.obtieneDatos(),p.obtieneLongitud(), 0,(struct sockaddr *)&direccionForanea,&clilen);
	// COPIAR A INET
   	memcpy(inet, &direccionForanea.sin_addr.s_addr, 4);

   	//printf("\n");
	p.inicializaIp((char*)inet);
	p.inicializaPuerto(ntohs(direccionForanea.sin_port));
	
	return res;
	
}
int SocketMulticast::envia(PaqueteDatagrama & p, unsigned char ttl){

	direccionForanea.sin_family = PF_INET;
   	direccionForanea.sin_addr.s_addr = inet_addr(p.obtieneDireccion());
   	direccionForanea.sin_port = htons(p.obtienePuerto());

	int val = setsockopt(s, IPPROTO_IP, IP_MULTICAST_TTL, (void *) &ttl, sizeof(ttl));
	if(val<0){
		printf("error en setsockopt \n");
	}
	return sendto(s, (char *)p.obtieneDatos(),p.obtieneLongitud(), 0, (struct sockaddr *) &direccionForanea, sizeof(direccionForanea));
	
}
//Se une a un grupo multicast, recibe la IP multicast
void SocketMulticast::unirseGrupo(char * multicastIP){

	struct ip_mreq multicast;
	multicast.imr_multiaddr.s_addr = inet_addr(multicastIP); /* Dirección IP del grupo multicast */
	multicast.imr_interface.s_addr = htonl(INADDR_ANY);  /* Dirección IP de la interfaz local IP */
	setsockopt(s,IPPROTO_IP,IP_ADD_MEMBERSHIP,(void *)&multicast,sizeof(multicast));
	//ADD MEMBERSHIP ES PARA UNIRSE
	
}
//Se sale de un grupo multicast, recibe la IP multicast
void SocketMulticast::salirseGrupo(char * multicastIP){

	struct ip_mreq multicast;
	multicast.imr_multiaddr.s_addr = inet_addr(multicastIP);
	multicast.imr_interface.s_addr = htonl(INADDR_ANY);
	setsockopt(s,IPPROTO_IP,IP_DROP_MEMBERSHIP,(void *)&multicast,sizeof(multicast));
	//IP DROP MEMBERSHIP ES PARA SALIRSE DEL GRUPO	
}

SocketMulticast::~SocketMulticast()
{
	//printf("%s\n", "Se cerro");
    close(s);
}
