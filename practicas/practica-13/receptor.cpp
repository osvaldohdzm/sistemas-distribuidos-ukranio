//receptor recibe como parámetros la IP de multicast y el puerto en el que escucha. 
// el recpetor debe imprimir la IP y puerto de quien le ha enviado el mensaje multicast, así como la cadena recibida.

#include <bits/stdc++.h>
#include "PaqueteDatagrama.h"
#include "SocketMulticast.h"
#include "SocketDatagrama.h"
#include <cstring>

using namespace std;

int main(){

	//char* ip_multicast;
	int puerto;
	int recibe;
	char datos[20];
	char resp[20];


	PaqueteDatagrama p(sizeof(char)*20);

	//cin >> puerto;

	SocketMulticast socket_multicast(7200);

	socket_multicast.unirseGrupo("224.0.0.1");

	recibe= socket_multicast.recibe(p);

	printf("Direccion: %d.%d.%d.%d\n", (unsigned char)p.obtieneDireccion()[0], (unsigned char)p.obtieneDireccion()[1], (unsigned char)p.obtieneDireccion()[2], (unsigned char)p.obtieneDireccion()[3] );
	printf("Puerto: %d\n", p.obtienePuerto());

	memcpy(datos, p.obtieneDatos(), sizeof(datos));

	cout << "Cadena recibida: " << datos << endl;
	memcpy(resp, "recibida", sizeof(resp));
	char aux[16]="";
	char *ip3 = p.obtieneDireccion();

	strcat(aux, to_string((int)(unsigned char)ip3[0]).c_str());
	strcat(aux, ".");
	strcat(aux, to_string((int)(unsigned char)ip3[1]).c_str());
	strcat(aux, ".");
	strcat(aux, to_string((int)(unsigned char)ip3[2]).c_str());
	strcat(aux, ".");
	strcat(aux, to_string((int)(unsigned char)ip3[3]).c_str());


	printf("%s\n", aux);




	return 0;
}