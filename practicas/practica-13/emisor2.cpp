// parámetros en línea de comandos la IP de multicast, el puerto, el valor de TTL y una cadena encerrada entre comillas

#include "SocketMulticast.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <bits/stdc++.h>
using namespace std;

int main() {
  string ipMulticast, cadena;
  int puerto, ttl;
  int numeros[2];
  numeros[0]=5;
  numeros[1]=8;
  int resutado=0;
  char* direccion;
  //cin >> ipMulticast >> puerto >> ttl >> cadena;
// IP de multicast, el puerto, el valor de TTL y una cadena
  puerto = 7200;
  ttl = 2;
  SocketMulticast s(puerto);
  PaqueteDatagrama a((char*)numeros, sizeof(int)*2, "224.0.0.1", puerto);
  s.envia(a, ttl);
  cout << "Enviado" << endl;
  
  SocketDatagrama sd(8080); 
  PaqueteDatagrama pd(sizeof(int)*4);
 
  while(true){
    
    sd.recibe(pd);

    memcpy(&resutado, pd.obtieneDatos(), sizeof(int));

    printf("Resutado es: -> %d \n", resutado);

    memcpy(direccion, pd.obtieneDireccion(), sizeof(char)*16);

    printf("Direccion -> %s\n",direccion );

  }
  
  
}
