#include "Solicitud.h"

using namespace std;

int main(int argc, char *argv[]){
    srand(time(NULL));
    Solicitud cliente;
    int resueltado=0; //variable para el resultado del servidor
    int cuentaCliente;
    if(argc<4)
        printf("Indicar IP y puerto del servidor y numero de depositos\n");    
    
    int suma_argumentos=1; //id de operación, ahorita no importa
    int argumentos[1];  //donde saco mi numero de depositos
    
    int numeroDepositos = atoi(argv[3]);
    printf("numero de depositos es: %d\n", numeroDepositos);
    printf("Saldo actual cliente: %d\n", resueltado);
    //se manda "numeroDepositos" veces la solicitud para incrementar
    for (int i = 0; i < numeroDepositos; ++i){
        //sacamos cantidad Aleatoria
        argumentos[0] = 1 + rand() % 9;
        cuentaCliente+=argumentos[0];
        //resueltado es el valor que incremento el servidor en servidor.cpp
        memcpy(&resueltado, cliente.doOperation(argv[1], atoi(argv[2]),suma_argumentos, (char *)argumentos ), sizeof(resueltado));       
    } 
    printf("Total que quiero incrementar: %d\n",cuentaCliente );
    printf("Total incrementado por el servidor :%d\n", resueltado);
    
    
    if(resueltado!=cuentaCliente){
        printf("*****La cuenta no incremento totalmente\n");
        resueltado=0;
        exit(0);
    }else{
        printf("Deposito realizado con exito\n");
        resueltado=0;
        exit(0);
    }
    
    

    
   // printf("Resultado: %d\n", resueltado);
    return 0;
}
