#include "Mensaje.h"
#include "Respuesta.h"
#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char*argv[]) {
  Respuesta respuesta(1415);
  int nbd=0;
  //printf("%s\n", "ESPERANDO...");
  while(true) {
    struct mensaje msg; //espera solicitud con la estructura del mensaje
    int argumento_recibido; //variable donde guardo el argumento recibido
    
    memcpy(&msg, respuesta.getRequest(), sizeof(struct mensaje)); //recibo el mensaje de solicitud y lo guardo en msg
    memcpy(&argumento_recibido, msg.arguments, sizeof(int)); //almacena en argumento_recibido el valor del deposito a realizar
    nbd+=argumento_recibido;
    memcpy(msg.arguments, &nbd, sizeof(int));//almanceno en msg la respuesta del cliente
    printf("Suma en servidor  %d\n", nbd);
    respuesta.sendReply(msg.arguments); //se envia al cliente la respuesta

    }
}
