#ifndef __SOCKETMULTICAST_H__
#define __SOCKETMULTICAST_H__
#include "PaqueteDatagrama.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string>
#include <unistd.h>

using namespace std;

class SocketMulticast{
	public:
		SocketMulticast(int);
		~SocketMulticast();
		int recibe(PaqueteDatagrama & p);
		int envia(PaqueteDatagrama & p, unsigned char ttl);
		//Se une a un grupo multicast, recibe la IP multicast
		void unirseGrupo(char *);
		//Se sale de un grupo multicast, recibe la IP multicast
		void salirseGrupo(char *);
	private:
		int s; //ID socket
		struct sockaddr_in direccionLocal;
		struct sockaddr_in direccionForanea;
}; 

#endif