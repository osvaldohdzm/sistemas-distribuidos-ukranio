#include "Mensaje.h"
#include "Respuesta.h"
#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char*argv[]) {
    Respuesta respuesta(7200);
    printf("%s\n", "ESPERANDO...");
    while(true) {

      struct mensaje msg; //espera solicitud con la estructura del mensaje
      int argumento_recibido[2]; 
      int id, resultado;
      memcpy(&msg, respuesta.getRequest(), sizeof(struct mensaje));

      memcpy(argumento_recibido, msg.arguments, 2*sizeof(int));

       printf("Los argumentos recibidos son:  %d  y %d \n", argumento_recibido[0], argumento_recibido[1]);
       id = msg.operationId;
       printf("EL id recibido es: %d\n",id );

        if (id == 1){
            resultado = argumento_recibido[0] + argumento_recibido[1];
        }else if(id == 0){
            resultado =  argumento_recibido[0] - argumento_recibido[1];
        }

        printf("Resultado es: %d\n",resultado );

        memcpy(msg.arguments, &resultado, sizeof(int));

        respuesta.sendReply(msg.arguments);
    }
}
