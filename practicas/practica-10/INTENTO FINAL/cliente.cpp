#include "Solicitud.h"
#include <chrono>

using namespace std;
using namespace std::chrono;

int main(int argc, char *argv[]){
    Solicitud cliente;
    int resueltado;
    if(argc<3)
        printf("Indicar IP y puerto del servidor\n");    
    int suma_argumentos=1;
    int argumentos[2];
    argumentos[0] = 10;
    argumentos[1] = 5;
   
    memcpy(&resueltado, cliente.doOperation(argv[1], atoi(argv[2]),suma_argumentos, (char *)argumentos), sizeof(resueltado));
    printf("Resultado: %d\n", resueltado);
    return 0;
}