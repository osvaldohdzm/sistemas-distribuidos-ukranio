#include "Solicitud.h"
#include <chrono>

using namespace std;
using namespace std::chrono;

int main(int argc, char *argv[]){
    Solicitud cliente;
    int resultado;
    int numSolicitudes;
    if(argc<4)
        printf("Indicar IP y puerto del servidor y numero de Solicitud\n");    
    int suma_argumentos=1;
    int argumentos[2];
    argumentos[0] = 10;
    argumentos[1] = 5;
    numSolicitudes = atoi(argv[3]);
    for (int i = 0; i < numSolicitudes; ++i)
    {
        memcpy(&resultado, cliente.doOperation(argv[1], atoi(argv[2]),suma_argumentos, (char *)argumentos), sizeof(resultado));
    }
    
   // printf("Resultado: %d\n", resultado);
    return 0;
}