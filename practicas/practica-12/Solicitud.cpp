#include "Solicitud.h"
#include <cstdlib>
#include <cstring>


Solicitud::Solicitud(){
    //socketlocal;
}
char * Solicitud::doOperation(char *IP, int puerto, int operationId, char *arguments, int numeroSolicitudes) {
    int res;
    struct mensaje msj;
    //size_t tam = 2*sizeof(int) + 16*sizeof(char) + strlen(arguments);

    msj.messageType = 0;
    msj.requestId = 1;
    msj.operationId = operationId;
    memcpy(msj.arguments, arguments, TAM_MAX_DATA);

    //size_t tam2 = 2*sizeof(int);
    PaqueteDatagrama datagrama((char *)&msj, sizeof(struct mensaje), IP, puerto);
    socketlocal.envia(datagrama);

    PaqueteDatagrama databack(sizeof(struct mensaje));

    for (int i = 0; i < numeroSolicitudes; i++){
        int rec = socketlocal.recibeTimeout(databack, 2, 0);
        printf("%d\n", rec);

        if ( rec > 0){// el timeout será 2.5 segundos
            printf("RECIBIDO CON EXITO\n");
            memcpy(&msj_databack, databack.obtieneDatos(), sizeof(struct mensaje));
            return msj_databack.arguments;

        }else if (rec == -1){
            printf ("REENVIANDO SOLICITUD %d\n", i+1);
            socketlocal.envia(datagrama);
            
        }
    }
        
    return msj_databack.arguments;
}
