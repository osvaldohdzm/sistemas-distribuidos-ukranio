#ifndef RECTANGULO_H_
#define RECTANGULO_H_

#include "Coordenada.h"

class Rectangulo
{
	private:
		Coordenada superiorIzq;
		Coordenada inferiorDer;
	public:
		Rectangulo();
		Rectangulo(Coordenada c1, Coordenada c2);
		Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer);
		void imprimeEsq();
		double obtieneArea();
		Coordenada obtieneSupIzq();
		Coordenada obtieneInfDer();
};

#endif
