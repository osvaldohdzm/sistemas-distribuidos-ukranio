#include "Rectangulo.h"
#include <iostream>

using namespace std;

Rectangulo::Rectangulo() : superiorIzq(0,0), inferiorDer(0,0)
{ }
//Rectangulo rectangulo1(Coordenada(2,3),Coordenada(5,1));
Rectangulo::Rectangulo(Coordenada c1, Coordenada c2)
{ 
	superiorIzq = c1;
	inferiorDer =  c2;
}

Rectangulo::Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer):superiorIzq(xSupIzq, ySupIzq), inferiorDer(xInfDer, yInfDer)
{ }

void Rectangulo::imprimeEsq()
{
	cout << "Para la esquina superior izquierda.\n";
	cout << "x = " << superiorIzq.obtenerX() << " y = " << superiorIzq.obtenerY() << endl;
	cout << "Para la esquina inferior derecha.\n";
	cout << "x = " << inferiorDer.obtenerX() << " y = " << inferiorDer.obtenerY() << endl;
}

double Rectangulo::obtieneArea()
{	
	double x1,x2,y1,y2;
	x1= superiorIzq.obtenerX();//2
	y1= superiorIzq.obtenerY();//3
	
	x2= inferiorDer.obtenerX();//5
	y2= inferiorDer.obtenerY();//1
	
	return (x2-x1)*(y1-y2);
}


