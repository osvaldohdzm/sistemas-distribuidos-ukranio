#include "Coordenada.h"
#include <math.h>
#include <iostream>

using namespace std;

/*Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy)

{ }*/

Coordenada::Coordenada(double xx, double yy){

	x=xx*cos(yy);
	y=xx*sin(yy);
}


double Coordenada::obtenerX()
{
return x;
}

double Coordenada::obtenerY()
{
return y;
}

