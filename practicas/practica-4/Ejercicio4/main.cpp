#include <iostream>

#include "Coordenada.h"
#include "Rectangulo.h"

using namespace std;

int main( )

{


	//Rectangulo rectangulo1(2,3,5,1);
	Rectangulo rectangulo1(Coordenada(3.6056,0.9828),Coordenada(5.0990,0.1974));
	//double ancho, alto;

	cout << "Calculando el área de un rectángulo dadas sus coordenadas en un plano cartesiano:\n";
	rectangulo1.imprimeEsq();

	/*alto = rectangulo1.obtieneSupIzq().obtenerY() - rectangulo1.obtieneInfDer().obtenerY();
	ancho = rectangulo1.obtieneInfDer().obtenerX() - rectangulo1.obtieneSupIzq().obtenerX();*/
	cout << "El área del rectángulo es = " << rectangulo1.obtieneArea() << endl;

	//cout << "El área del rectángulo es = " << (int)rectangulo1.obtieneArea() << endl;
	return 0;
}
