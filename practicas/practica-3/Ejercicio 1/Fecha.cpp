#include "Fecha.h"
#include <iostream>
#include <stdlib.h>

using namespace std;
/*
Fecha::Fecha(int *dd, int *mm, int *aaaa){
	this->mes = *mm;this->dia = *dd;this->anio = *aaaa;
}
*/

Fecha::Fecha(int *dd, int *mm, int *aaaa): dia(*dd), mes(*mm), anio(*aaaa){
	if(dia > 1 && dia < 31 && anio > 0 && anio < 2019){
		cout << "Valor correcto para el mes!\n";
		exit(1);
	}else{
		cout << "Valor incorrecto para el mes!\n";
		exit(1);
	}

}

void Fecha::inicializaFecha(int *dd, int *mm, int *aaaa){
	this->anio = *aaaa;
	this->mes = *mm;
	this->dia = *dd;
	return;
}

void Fecha::muestraFecha(){
	if(dia > 1 && dia < 31 && anio > 0 && anio < 2019){
		cout << "Valor correcto para el mes!\n";
		exit(1);
	}else{
		cout << "Valor incorrecto para el mes!\n";
		exit(1);
	}
	//cout << "La fecha es(dia-mes-año): " << dia << "-" << mes << "-" << anio << endl;
	return;   
}
