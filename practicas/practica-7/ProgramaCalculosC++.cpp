#include <iostream>	
#include <math.h>
using namespace std;

int main()
{
	double i=0;
	double seno,conseno,tangente,logaritmo,raiz;
	
	while(i<650000){
		seno += sin(i);
		conseno += cos(i);
		tangente += tan(i);
		logaritmo += log(i);
		raiz += sqrt(i);
		i++;
	}/*While*/
	
	return 0;
}/*MAIN*/