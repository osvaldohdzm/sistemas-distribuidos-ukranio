import math;

seno = 0
coseno = 0
tangente = 0
logaritmo = 1
raizCua = 0

for j in range(500000): 
	seno += math.sin(j)
	coseno += math.cos(j)
	tangente += math.tan(j)
	raizCua += math.sqrt(j)
	if j >= 1:	
		logaritmo += math.log(j)