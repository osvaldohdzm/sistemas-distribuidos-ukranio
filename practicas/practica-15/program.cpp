#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream> 
#include <list> 
#include <iterator> 
#include <vector> 
#include <fstream>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
using namespace std;


std::vector<std::string> readFile(char* file_name)
{
std::vector<std::string> lines;
    std::ifstream input(file_name);
    std::string line;

     if(input.fail()){
        perror(file_name);
		exit(-1);
    }

    while (std::getline(input, line))
    {
        lines.push_back(line + "\n");
    }
    
    return lines;

}

/* cliente que lee un archivo que contiene un único registro, y lo manda en un mensaje UDP hacia el servidor de base 
de datos*/
int main(int argc, char* argv[]){

	char buffer[BUFSIZ];//region de memoria para el almacenamiento temporal de datos
	int nbytes, origen;
    char *p;
    int num;
    errno = 0;
    int n;

	if(argc != 3){
		printf("Forma de uso: ./cliente nombre_del_archivo.txt num_registros\n");
		exit(-1);
	}
    

    long conv = strtol(argv[2], &p, 10);
    if (errno != 0 || *p != '\0') {
    printf("%s No es un número\n",argv[2]);
    exit(-1);
} 
else {

    std::vector<std::string> registros = readFile(argv[1]);
    num = conv;
    if (num > registros.size())
    {
        printf("%s Es mayor al número de registros en el archivo\n",argv[2]);
        exit(-1);
    }

    SocketDatagrama socket = SocketDatagrama(7000);

    char *cstr;
    for (int i =0; i< num;i++)
    {
        cstr = registros[1].c_str();
        PaqueteDatagrama datagrama = PaqueteDatagrama(buffer, 31,"127.0.0.1", 7200);
	    n = socket.envia(datagrama);
	    cout << "Tamaño de envio: "<< n << endl;
    }	
}    
	

	
	return 0;
}