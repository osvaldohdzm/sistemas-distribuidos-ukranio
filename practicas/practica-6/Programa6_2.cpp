#include <iostream>
#include <random>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

//compilar g++ -std=c++11 nombre_archivo.cpp

using namespace std;

class Coordenada{
	private:
		double x;
		double y;
	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
};

class PoligonoIrreg{
	private:
		vector<Coordenada> v; //vector de objetos coordenada
		static int numVertices;
		static double magnitud;	//Variable miembro Magnitud
	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada c); //Añadir un vertice 
		void imprimeVertices(); 		//imprimir conjunto de vertices del poligono
		static int getnumVertices();
		Coordenada getVertices(Coordenada, int);
		static double getMagnitud(double, double);  
		bool ordenaA(double, double);
		void ordenaAscendente();
};

int main(){
	srand(time(NULL));
	double X, Y;
	PoligonoIrreg poligono;
	int n = 10;	
	for(int i = 0; i < 10; i++){
		X = -100 * rand() % 100;
		Y = -100 * rand() % 100;
		//3 decimales 		
		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(3);
		
		//Funciones de C++ 11 para determinado rango de numeros
		std::random_device rd;
		std::mt19937 gen(rd());
		//Rango del 0.001 al 0.999
		std::uniform_real_distribution<> dist(0.001, 0.999);
				 
		Coordenada c(X + dist(gen), Y + dist(gen));	
		poligono.anadeVertice(c);	
	}
	poligono.imprimeVertices();
	poligono.ordenaAscendente();				
	return 0;
}

double vec[100];

bool ordenaB(double i, double j){ return ( i < j ); }

struct claseaux{
	bool operator() (int i, int j) { return ( i < j );}
}objetoaux;

double PoligonoIrreg::magnitud = 0;

Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){ }

double Coordenada::obtenerX(){return x;}

double Coordenada::obtenerY(){return y;}

PoligonoIrreg::PoligonoIrreg(){vector<Coordenada> v;}

void PoligonoIrreg::anadeVertice(Coordenada c){v.push_back(c);}

void PoligonoIrreg::ordenaAscendente(){
	vector<Coordenada>::iterator i;	
	double auxiliar[v.size()];
	double auxiliar1[v.size()];
	double auxiliar3[v.size()];
	double finalres[v.size()];
	
	int fila = 0; 
	
	vector<double> auxiliar2(vec, vec + v.size());
	std::sort(auxiliar2.begin(), auxiliar2.end());
	std::sort(auxiliar2.begin(), auxiliar2.end(), ordenaB);
	std::sort(auxiliar2.begin(), auxiliar2.end(), objetoaux);
	
	for(vector<double>::iterator a = auxiliar2.begin(); a != auxiliar2.end(); a++){
		finalres[fila] = *a;
		fila++; 		
	}
	
	cout << "\n\nCoordenadas Ordenadas(Magnitud)" << endl;
	int ite = 0, n = 0, l=0;
	for(int j = 0; j < fila; j++){	
		vector<Coordenada>::reverse_iterator i;	
		for(i = v.rbegin(); i != v.rend(); i++){
			Coordenada c = *i;			
			if(finalres[j] == getMagnitud(c.obtenerX(), c.obtenerY()))	
				cout << "x = " << c.obtenerX() << " y = " << c.obtenerY() << ", magnitud = " << finalres[j] << endl; 			}
		
	}	
}

double PoligonoIrreg::getMagnitud(double x, double y){
	magnitud = sqrt((x * x) + (y * y));
	return magnitud;
}

void PoligonoIrreg::imprimeVertices(){
	cout << "Las coordenadas del Poligono son: " << endl;
	int indices = 1;
	//El vector iterador tiene que ser del mismo tipo que el vetor Coordenada
	vector<Coordenada>::reverse_iterator i;	
	for(i = v.rbegin(); i != v.rend(); i++){
		magnitud = 0;		
		//Como *i tiene los elementos de Coordenada, los igualé para poder obtener los elementos de cada Coordenada
		Coordenada c = *i;
		cout << "x" << indices <<"= " << c.obtenerX() << "  y" << indices <<" = " << c.obtenerY() <<" ,  magnitud = " 			<< getMagnitud(c.obtenerX(), c.obtenerY()) << endl;
		vec[indices] = getMagnitud(c.obtenerX(), c.obtenerY()); 
		indices += 1;	
	}
}
