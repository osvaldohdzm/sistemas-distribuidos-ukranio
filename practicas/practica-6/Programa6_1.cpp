#include <iostream>
#include <random>
#include <vector>
#include <stdlib.h>
#include <math.h>


//compilar g++ -std=c++11 nombre_archivo.cpp

using namespace std;

class Coordenada{
	private:
		double x;
		double y;
	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
};

class PoligonoIrreg{
	private:
		vector<Coordenada> v; //vector de objetos coordenada
		static int numVertices;
		static double magnitud;	//Variable miembro Magnitud
	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada c); //Añadir un vertice 
		void imprimeVertices(); 		//imprimir conjunto de vertices del poligono
		static int getnumVertices();
		static double getMagnitud(double, double);
};

int main(){
	srand(time(NULL));
	double X, Y;
	PoligonoIrreg poligono;	
	for(int i = 0; i < 10; i++){
		X = -100 * rand() % 100;
		Y = -100 * rand() % 100;
		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(3);
		
		//Funciones de C++ 11 para determinado rango de numeros
		std::random_device rd;
		std::mt19937 gen(rd());
		//Rango del 0.001 al 0.999
		std::uniform_real_distribution<> dist(0.001, 0.999);
				
		Coordenada c(X + dist(gen), Y + dist(gen));	
		poligono.anadeVertice(c);	
	}			
	poligono.imprimeVertices();
	return 0;
}

double PoligonoIrreg::magnitud = 0;

Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){ }

double Coordenada::obtenerX(){return x;}

double Coordenada::obtenerY(){return y;}

PoligonoIrreg::PoligonoIrreg(){vector<Coordenada> v;}

void PoligonoIrreg::anadeVertice(Coordenada c){v.push_back(c);}

double PoligonoIrreg::getMagnitud(double x, double y){
	magnitud = sqrt((x * x) + (y * y));
	return magnitud;
}

void PoligonoIrreg::imprimeVertices(){
	cout << "Las coordenadas del Poligono son: " << endl;
	int indices = 1;
	//El vector iterador tiene que ser del mismo tipo que el vetor Coordenada
	vector<Coordenada>::reverse_iterator i;	
	for(i = v.rbegin(); i != v.rend(); i++){
		magnitud = 0;		
		//Como *i tiene los elementos de Coordenada, los igualé para poder obtener los elementos de cada Coordenada
		Coordenada c = *i;

		cout << "x" << indices <<"= " << c.obtenerX() << "  y" << indices <<" = " << c.obtenerY() <<" ,  magnitud = " 			<< getMagnitud(c.obtenerX(), c.obtenerY()) << endl;
		indices += 1;
	}
}
